'use strict';

import * as HttpStatus from "http-status-codes";
import * as responseParser from "../utils/responseParser";
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');

var mobileSubscriptions = [
  {
    "id": "12345",
    "name": "DNA Max 4G Rajaton",
    "internet": "4G-verkon maksiminopeus",
    "eu_data": "20 Gt/kk",
    "calls_sms": "Rajaton",
    "price": "49,90 €/kk",
    "username": "Heikki Koskinen",
    "mobile_number": "0400198911"
  },
  {
    "id": "12346",
    "name": "DNA Super 4G Rajaton",
    "internet": "300 Mbit/s",
    "eu_data": "15 Gt/kk",
    "calls_sms": "Rajaton",
    "price": "31,90 €/kk/12 kk",
    "username": "Heikki Koskinen",
    "mobile_number": "0400198912",
  },
  {
    "id": "12347",
    "name": "DNA Nopea 4G Rajaton",
    "internet": "100 Mbit/s",
    "eu_data": "10 Gt/kk",
    "calls_sms": "Rajaton",
    "price": "27,90 €/kk/12 kk",
    "username": "Heikki Koskinen",
    "mobile_number": "0400198913",
  }
]

var broadbandSubscriptions = [
  {
    "id": "12348",
    "name": "DNA Koti Max 4G Rajaton",
    "internet": "4G-verkon maksiminopeus",
    "price": "49,90 €/kk",
    "username": "Heikki Mikkonen",
    "address": "Insinöörinkatu 88 B 20m, 33720, Tampere Finland"
  },
  {
    "id": "12349",
    "name": "DNA VDSL Super Rajaton",
    "internet": "300 Mbit/s",
    "price": "31,90 €/kk/12 kk",
    "username": "Heikki Mikkonen",
    "address": "Insinöörinkatu 88 B 20m, 33720, Tampere Finland"
  }
]



/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  GetMobileSubscription: GetMobileSubscription,
  GetMobileSubscriptions: GetMobileSubscriptions,
  GetBroadbandSubscription: GetBroadbandSubscription,
  GetBroadbandSubscriptions: GetBroadbandSubscriptions,
};

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function GetMobileSubscription(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  const subId = req.swagger.params.subId ? req.swagger.params.subId.value : "";
  
  const found = mobileSubscriptions.find(element => {
    return element.id == subId
  })

  var result = {
    body: {data: found},
    code: 200,
  }
  responseParser.parseResponse(result, res);
}

function GetMobileSubscriptions(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}

  var result = {
    body: {data: mobileSubscriptions},
    code: 200,
  }
  responseParser.parseResponse(result, res);
}

function GetBroadbandSubscription(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  const subId = req.swagger.params.subId ? req.swagger.params.subId.value : "";
  
  const found = broadbandSubscriptions.find(element => {
    return element.id == subId
  })

  var result = {
    body: {data: found},
    code: 200,
  }
  responseParser.parseResponse(result, res);
}

function GetBroadbandSubscriptions(req, res) {
  // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}

  var result = {
    body: {data: broadbandSubscriptions},
    code: 200,
  }
  responseParser.parseResponse(result, res);
}
