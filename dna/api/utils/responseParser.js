"use strict";

function parseResponse(response, responseObject) {
    parseResponseData(response.code, response.body, responseObject);
}
exports.parseResponse = parseResponse;
function parseResponseData(code, responseBody, responseObject) {
    responseObject
        .header("content-type", "application/json")
        .status(code);
    if (responseBody !== undefined) {
        responseObject.end(JSON.stringify(responseBody));
    }
    else {
        responseObject.end();
    }
}
exports.parseResponseData = parseResponseData;