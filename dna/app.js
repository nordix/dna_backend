'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
const swaggerUi = require('swagger-ui-express');
module.exports = app; // for testing



var config = {
  appRoot: __dirname,
  configDir: `${__dirname}/config`,
  swaggerSecurityHandlers: {
    bearer: function BearerSecurityHandler (req, auth, token, callback) {
    if (req.path !== undefined && (req.path.includes("auth") || req.path.includes("revokeauth") )) {
      callback(null);
    } else if (token === undefined || token === '') {
      callback(new Error("Access denied, token expired or not valid"))
    } else {
		  console.log('To do: check valility of token with token provider')
      callback(null);
    }
	}
  }
}

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);
  app.use(swaggerExpress.runner.swaggerTools.swaggerUi());

  var port = process.env.PORT || 10010;
  app.listen(port);
  console.log(`swagger interface under: http://127.0.0.1:${port}/`);
  console.log(`swagger docs under: http://127.0.0.1:${port}/docs`);
  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('try this:\ncurl http://127.0.0.1:' + port + '/hello?name=Scott');
  }
});
